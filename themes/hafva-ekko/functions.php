<?php

 define('THEMEPATH', get_stylesheet_directory());

 add_action( 'wp_enqueue_scripts', 'kd_enqueue_parent_theme_style', 5 );
 if ( ! function_exists( 'kd_enqueue_parent_theme_style' ) ) {
     function kd_enqueue_parent_theme_style() {
         wp_enqueue_style( 'bootstrap' );
         wp_enqueue_style( 'keydesign-style', get_template_directory_uri() . '/style.css', array( 'bootstrap' ) );
         wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array('keydesign-style'), wp_get_theme()->get('Version') );

         wp_enqueue_script( 'child-script', get_stylesheet_directory_uri() . '/assets/js/main.js', array('jquery'), wp_get_theme()->get('Version'), true );
     }
 }

 add_action( 'after_setup_theme', 'kd_child_theme_setup' );
 if ( ! function_exists( 'kd_child_theme_setup' ) ) {
     function kd_child_theme_setup() {
         load_child_theme_textdomain( 'ekko', get_stylesheet_directory() . '/languages' );
     }
 }

 // -------------------------------------
 // Edit below this line
 // -------------------------------------

require_once 'elements/elements.php';
require_once 'admin-options.php';



// Fix for aws
function as3cf_filter_get_post_metadata( $metadata, $object_id, $meta_key, $single ) {
    $meta_filter = array('_wpb_shortcodes_custom_css', '_wpb_post_custom_css');
    if ( isset( $meta_key ) && $single && in_array($meta_key, $meta_filter)) {
        remove_filter( 'get_post_metadata', 'as3cf_filter_get_post_metadata', 100 );
        $metadata = get_post_meta( $object_id, $meta_key, $single );
        add_filter('get_post_metadata', 'as3cf_filter_get_post_metadata', 100, 4);
        $metadata = apply_filters( 'as3cf_filter_post_local_to_s3', $metadata );
    }
    return $metadata;
}
add_filter( 'get_post_metadata', 'as3cf_filter_get_post_metadata', 100, 4 );


// Add ssn for checkout
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
  unset($fields['billing']['billing_company']);
  return $fields;
}

add_action('admin_head', 'my_custom_admin_styles');

function my_custom_admin_styles() {
  // just add the css selectors below to hide each field as required
  echo '
    <style>
      #order_data .order_data_column .form-field._billing_company_field { display: none; }
    </style>
  ';
}

function cart_notice() {
  global $wpdb, $woocommerce;

  $shipping = \WC_Shipping_Zones::get_zones();

  foreach($shipping as $zone){
    foreach ($zone['zone_locations'] as $location){
      if($location->code == 'SE') {
        foreach($zone['shipping_methods'] as $method){
          if (is_a( $method, 'WC_Shipping_Free_Shipping' )) {
            
            
            $maximum = intval($method->min_amount);
            break;
          }
        }
      }
    }
  }

  $current = WC()->cart->subtotal;
  if ( $current < $maximum ) {
      echo '<div class="woocommerce-info">Köp för ' . ($maximum - $current) . ' SEK till så får du fraktfri leverans.</div>';
  }
}
add_action( 'woocommerce_before_cart', 'cart_notice' );
add_action( 'kco_wc_before_checkout_form', 'cart_notice' );


function dataLayer_google_gtm($order_id) {
  if (!is_order_received_page()){
    return;
  }

  // Lets grab the order
  $order = wc_get_order( $order_id );

  // Products
  $products = $order->get_items();
  ?>

  <!-- Event snippet for Försäljning - Hafva conversion page -->
  <script> 
  gtag('event', 'conversion', { 
    'send_to': 'AW-852947904/5qdbCPDx9ZYBEMDn25YD', 
    'value': <?php echo number_format(($order->get_subtotal() + $order->get_total_tax()), 2, ".", ""); ?>, 
    'currency': 'SEK', 
    'transaction_id': '<?= $order->get_id() ?>' 
  }); </script>
  <?php
}
add_action( 'woocommerce_thankyou', 'dataLayer_google_gtm', 20 );

function add_google_scripts_header() {
  ?>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-P2HVB7Q');</script>
    <!-- End Google Tag Manager -->

    <!-- Global site tag (gtag.js) - Google Ads: 852947904 --> 
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-852947904"></script> 
    <script> 
        window.dataLayer = window.dataLayer || []; function gtag(){
            dataLayer.push(arguments);
        } 
        gtag('js', new Date()); gtag('config', 'AW-852947904');
    </script>
  <?php
}

function add_google_scripts_body() {
  ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P2HVB7Q"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
  <?php
}

add_action('wp_head', 'add_google_scripts_header');
add_action( 'wp_body_open', 'add_google_scripts_body' );

function add_facebook_metatag_header() {
  ?>
    <meta name="facebook-domain-verification" content="c5w7ehlqu8fmeb8nxv43mj5fhrxoj8" />
    <!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '966997190965657');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=966997190965657&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->
  <?php
}
add_action('wp_head', 'add_facebook_metatag_header');


add_action( 'wp_enqueue_scripts', 'add_klarna_scripts', 11 );
function add_klarna_scripts() {
  if ( is_plugin_active( 'klarna-onsite-messaging-for-woocommerce/klarna-onsite-messaging-for-woocommerce.php' ) ) {
    $settings = get_option( 'woocommerce_kco_settings' );
    $client_id = '';
    $uci = '';
    
    if ( isset( $settings['data_client_id'] ) ) {
      $client_id = $settings['data_client_id'];
    }

    if ( isset( $settings['onsite_messaging_uci'] ) ) {
      $uci = $settings['onsite_messaging_uci'];
    }

    if ( !is_product() && !is_cart() && !( ! empty( $post ) && has_shortcode( $post->post_content, 'onsite_messaging' ) ) ) {
      if ( ! empty( $client_id ) ) {
        wp_enqueue_script( 'klarna-onsite-messaging' );
      } elseif ( ! empty( $uci ) ) {
        wp_enqueue_script( 'onsite_messaging_script' );
      }
      wp_enqueue_script( 'klarna_onsite_messaging' );
    }
  }
}

/**
 * Filter products by type
 *
 * @access public
 * @return void
 */
function wpa104537_filter_products_by_featured_status() {

  global $typenow, $wp_query;

 if ($typenow=='product') :

    $info_taxonomy = get_taxonomy('product_visibility');
    $terms = get_terms(
      $info_taxonomy->name, // your custom taxonomy slug
        array(
            // get all the terms, even empty ones so we can see what items have 0
            'hide_empty' => false,
        )
    );
    $featuredCount = 0;
    foreach ($terms as $term) {
      if ( $term->slug != 'featured' )
        continue;
        
      $featuredCount = $term->count;
    }

     // Featured/ Not Featured
     $output .= "<select name='featured_status' id='dropdown_featured_status'>";
     $output .= '<option value="">Filtrera efter '.lcfirst($info_taxonomy->label).'</option>';

     $output .="<option value='featured' ";
     if ( isset( $_GET['featured_status'] ) ) $output .= selected('featured', $_GET['featured_status'], false);
     $output .=">".__( 'Featured', 'woocommerce' )." ($featuredCount)</option>";

     $output .="<option value='normal' ";
     $notFeaturedCount = array_sum((array)wp_count_posts( 'product' )) - $featuredCount;
     if ( isset( $_GET['featured_status'] ) ) $output .= selected('normal', $_GET['featured_status'], false);
     $output .=">Inte ".lcfirst(__( 'Featured', 'woocommerce' ))." ({$notFeaturedCount})</option>";

     $output .="</select>";

     echo $output;
 endif;
}

add_action('restrict_manage_posts', 'wpa104537_filter_products_by_featured_status');

/**
 * Filter the products in admin based on options
 *
 * @access public
 * @param mixed $query
 * @return void
 */
function wpa104537_featured_products_admin_filter_query( $query ) {
  global $typenow;

  if ( $typenow == 'product' ) {

      // Subtypes
      if ( ! empty( $_GET['featured_status'] ) ) {
          if ( $_GET['featured_status'] == 'featured' ) {
              $query->query_vars['tax_query'][] = array(
                  'taxonomy' => 'product_visibility',
                  'field'    => 'slug',
                  'terms'    => 'featured',
              );
          } elseif ( $_GET['featured_status'] == 'normal' ) {
              $query->query_vars['tax_query'][] = array(
                  'taxonomy' => 'product_visibility',
                  'field'    => 'slug',
                  'terms'    => 'featured',
                  'operator' => 'NOT IN',
              );
          }
      }

  }

}
add_filter( 'parse_query', 'wpa104537_featured_products_admin_filter_query' );



// Add a custom field in admin product edit pages - inventory tab
add_action( 'woocommerce_product_options_stock_fields', 'add_product_options_stock_custom_field', 20 );
function add_product_options_stock_custom_field() {
    global $product_object, $post;

    woocommerce_wp_text_input( array(
        'id'          => '_backorder_text',
        'type'        => 'text',
        'label'       => __( 'Backorders text', 'woocommerce' ),
        'description' => __( 'Backorders text. Add a custom backorders text to be displayed when products are on backorders.', 'woocommerce' ),
        'desc_tip'    => true,
    ) );

    // jQuery: HIDE the fied if backorders are not enabled
    ?>
    <script type="text/javascript">
    jQuery( function($){
        var a = 'select#_backorders',
            b = 'p._backorder_text_field';

        if( $(a).val() === 'no' )
            $(b).hide();

        $(a).on('change blur', function(){
            if( $(a).val() === 'no' )
                $(b).hide();
            else
                $(b).show();
        });
    });
    </script>
    <?php
}

// Save the custom field value from admin product edit pages - inventory tab
add_action( 'woocommerce_process_product_meta', 'save_product_options_stock_custom_field', 20, 1 );
function save_product_options_stock_custom_field( $product_id ) {
    if ( isset( $_POST['_backorder_text'] ) )
        update_post_meta( $product_id, '_backorder_text', sanitize_text_field( $_POST['_backorder_text'] ) );
}

// Variations: Add a custom field in admin variation options inventory
add_action( 'woocommerce_variation_options_inventory', 'add_variation_settings_fields', 20, 3 );
function add_variation_settings_fields( $loop, $variation_data, $variation_post ) {

    woocommerce_wp_text_input( array(
        'id'            => '_backorder_text'.$loop,
        'name'          => '_backorder_text['.$loop.']',
        'value'         => get_post_meta( $variation_post->ID, '_backorder_text', true ),
        'type'          => 'text',
        'label'         => __( 'Backorders text', 'woocommerce' ),
        'description'   => __( 'Backorders text. Add a custom backorders text to be displayed when products are on backorders.', 'woocommerce' ),
        'desc_tip'      => true,
        'wrapper_class' => 'form-row form-row-first',
    ) );
}

// Variations: Save a custom field value from admin variation options inventory
add_action( 'woocommerce_save_product_variation', 'save_variation_settings_fields', 10, 2 );
function save_variation_settings_fields( $variation_id, $i ) {
    if( isset( $_POST['_backorder_text'][$i] ) )
        update_post_meta( $variation_id, '_backorder_text', sanitize_text_field( $_POST['_backorder_text'][$i] ) );
}




function backorder_message($product_id) {
  $product_custom_text = get_post_meta( $product_id, '_backorder_text', true );

  return (!empty($product_custom_text)) ? $product_custom_text : 'Beställningsvara 3-4 veckor';
}

add_filter( 'woocommerce_get_availability', 'custom_on_backorder_text', 10, 2 );
function custom_on_backorder_text( $availability, $product ) {
    $backorder_text = backorder_message($product->get_id());

    if( $availability['class'] === 'available-on-backorder' ) {
      $availability['availability'] = $backorder_text;
    }

    return $availability;
}
 
 
function woocommerce_custom_cart_item_name( $_product_title, $cart_item, $cart_item_key ) {
  $altmessage = backorder_message($cart_item['product_id']);

  if ( $cart_item['data']->backorders_require_notification() && $cart_item['data']->is_on_backorder( $cart_item['quantity'] ) ) {
    $_product_title .=  wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification custom">' . $altmessage . '</p>', $product_id ) );
  }

  return $_product_title;
}
add_filter( 'woocommerce_cart_item_name', 'woocommerce_custom_cart_item_name', 10, 3);