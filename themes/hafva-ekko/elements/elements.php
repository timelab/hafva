<?php
if (function_exists('vc_map_update')) {
    // Change maps settings
    vc_map_update( 'tek_map', array('html_template' => get_stylesheet_directory() . '/elements/map.php') ); 
    add_action( 'init', 'remove_tek_map_shortcodes',20 );
    function remove_tek_map_shortcodes() {
        remove_shortcode( 'tek_map' );
    }
}