<?php
/*
* ReduxFramework Options Config
*/

if ( ! class_exists( 'Redux' ) && ! class_exists('WooCommerce') ) {
    return;
}

add_action( 'after_setup_theme', 'include_admin_options', 1 );
function include_admin_options(){
    $opt_name = 'redux_ThemeTek';

    $fields = array(
        'shop-page' => array(
            array(
                'id' => 'tek-default-woocommerce-category-bg',
                'type' => 'media',
                'readonly' => false,
                'url' => true,
                'title' => esc_html__('Default category image', 'ekko'),
                'subtitle' => esc_html__('Default category background image in title bar', 'ekko')
            )
        ),
        'top-bar' => array(
            array(
                'id' => 'tek-top-bar-custom-text-position',
                'type' => 'button_set',
                'title' => __( 'Custom text position' , 'redux_docs_generator' ),
                'desc' => __( 'Placement on top-bar to be placed on right side or left side' , 'redux_docs_generator' ),
                'options' => array(
                    'tek-topbar-left-content' => 'Left',
                    'tek-topbar-center-content' => 'Center',
                    'tek-topbar-right-content' => 'Right',
                ),
                'default' => 'tek-topbar-right-content'
            ),
            array(
                'id' => 'tek-top-bar-custom-text',
                'type' => 'editor',
                'title' => esc_html__('Custom text', 'ekko'),
                'subtitle' => esc_html__('Custom text in top bar', 'ekko'),
                'args' => array(
                    'media_buttons' => false
                )
            )
        )
    );

    foreach ($fields as $tab => $field_arr) {
        foreach ($field_arr as $field) {
            Redux::set_field( $opt_name, $tab, $field );
        }
    }
}