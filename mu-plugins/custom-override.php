<?php

/**
 * OVERRIDE CTX FEED AVAILABILITY
 * Modify  Availability attribute value based on channel.
 *
 * @param $attribute_value
 * @param $product
 * @param $feed_config
 *
 * @return string
 */
function woo_feed_availability_attribute_value_modify($attribute_value, $product, $feed_config)
{
    if ('bestprice' === $feed_config['provider']) {
        if ('in stock' === $attribute_value) {
            return "Y";
        }

        return "N";
    }

  if ( 'skroutz' === $feed_config['provider'] ) {

    if ('in stock' === $attribute_value) {
      $in_stock_string = __('Delivery 1 to 3 days', 'woo-feed' );
    } else {
      $in_stock_string = __('Delivery up to 30 days', 'woo-feed' );
    }

    return $in_stock_string;
  }

    if ('pricerunner' === $feed_config['provider']) {
        if ('in stock' === $attribute_value) {
            return "Yes";
        }

        return "No";
    }

    if ('google' === $feed_config['provider'] || 'pinterest' === $feed_config['provider']) {
        if ('on backorder' === $attribute_value) {
            return 'in stock';
        } elseif('on_backorder' === $attribute_value) {
            return 'in_stock';
        }

        if( 'google' === $feed_config['provider'] ) {
            if( ! in_array( $attribute_value, [ 'in_stock', 'out_of_stock', 'on_backorder' ] ) ) {
                return 'in_stock';
            }
        } else if( ! in_array( $attribute_value, [ 'in stock', 'out of stock', 'on backorder' ] ) ) {
            return 'in stock';
        }
    }

    if ('facebook' === $feed_config['provider'] ) {
        if( 'on backorder' === $attribute_value ){
            return 'available for order';
        }else if( ! in_array( $attribute_value, [ 'in stock', 'out of stock', 'on backorder' ] ) ) {
            return 'in stock';
        }
    }

    return $attribute_value;
}

add_filter('woocommerce_order_item_get_formatted_meta_data', 'remove_backorder_from_eamil', 10, 1); 
function remove_backorder_from_eamil($formatted_meta) {
    foreach ($formatted_meta as $id => $meta) {
        if ($meta->key === "Restnoterad") {
            unset($formatted_meta[$id]);
        }
    }

    return $formatted_meta;
}